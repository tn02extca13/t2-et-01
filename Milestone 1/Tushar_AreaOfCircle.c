//A program to calculate the Area of a Circle

#include<stdio.h>
float main()
{
	float r,a,pi=3.142;
	printf("Enter radius of circle: ");
	scanf("%f",&r);
	a=pi*r*r;
	printf("Area of circle: %.2f",a);
}

/*

OUTPUT:

c:\Mycode>gcc areaofcircle.c

c:\Mycode>a.exe
Enter radius of circle: 5
Area of circle: 78.55
c:\Mycode>

*/