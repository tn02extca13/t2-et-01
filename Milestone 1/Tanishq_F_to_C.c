//A program to convert temperature in Farenheit(F) to Celcius(C)

#include<stdio.h>
float main()
{
	float F,C;
	printf("Enter temperature in farenheit: ");
	scanf("%f",&F);
	C=(F-32)*5/9;
	printf("Enter temperature in Celsius= %f",C);
}

/*
OUTPUT:

c:\Mycode>gcc f_to_c.c

c:\Mycode>a.exe
Enter temperature in farenheit: 10
Enter temperature in Celsius= -12.222222
c:\Mycode>

*/