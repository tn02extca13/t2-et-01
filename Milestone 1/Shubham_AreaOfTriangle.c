//A program to calculate the Area of a Triangle

#include<stdio.h>
float main()
{
	float b,h,a;
	printf("Enter base of triangle: ");
	scanf("%f",&b);
	printf("Enter height of triangle: ");
	scanf("%f",&h);
	a=(b*h)/2;
	printf("Area of triangle: %.2f",a);
}

/*
OUTPUT:

c:\Mycode>gcc areaoftriangle.c

c:\Mycode>a.exe
Enter base of triangle: 5
Enter height of triangle: 7
Area of triangle: 17.50
c:\Mycode>

*/