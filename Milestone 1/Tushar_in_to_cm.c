//A program to convert length entered in inches to centimeters(cm)

#include<stdio.h>
float main()
{
	float in,cm;
	printf("Enter lenght in inches: ");
	scanf("%f",&in);
	cm=2.54*in;
	printf("Length in Centimeters= %f",cm);
}

/*
OUTPUT:


c:\Mycode>gcc in_to_cm.c

c:\Mycode>a.exe
Enter lenght in inches: 5
Length in Centimeters= 12.700000
c:\Mycode>

*/