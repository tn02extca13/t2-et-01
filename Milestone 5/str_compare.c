//int str_compare(char *str1, char *str2) - if str1 is less than str2 the function should return -1 if str1 is greater than str2 it should return 1 and if both are equal then return 0

#include <stdio.h>

int str_compare(char* str1, char* str2, int* a);

int main ()
{
	char str1[10], str2[10];
	int a, p;
    
    printf("\nEnter 1st string: "); 
    gets(str1);
	printf("\nEnter 2nd string: "); 
    gets(str2);
	
	p = str_compare(str1,str2,&a);
	
	if (p > a)
	{
		printf("%s < %s\n",str1,str2);
		
	}
	else if (p < a)
	{
		printf("%s > %s\n",str1,str2);
	}
	else
	{
		printf("%s = %s\n",str1,str2);
	}
	
    return 0;
}

int str_compare(char* str1, char* str2, int* a)
{
	int r=0;
	
	while (*str1!='\0'|| *str2!='\0')
	{
		if (*str1 != *str2)
		{
			r = (int)(*str1 - *str2);
			break;
		}
		else
		{
			str1++;
			str2++;
		}
	}
	
	if (r > 0)
	{
		*a = r;
		return 1;
	}
	else if (r < 0)
	{
		*a = r;
		return 1;
	}
	else
		*a = r;
		return 0;
}