//int str_length(char *str) - returns the length of the string or -1 on error

#include <stdio.h>

int str_length(char *str);

int main() 
{
	char str[10];
	int i, s;
	printf("\nEnter a string: ");
	gets(str);
	
	s = str_length(str);
	
	if (s == 1)
	{
		printf("Error! Upto only 10 characters.\n");
	}
	else 
	{
		printf("The length of string: %d.\n", s);
	}
	return 0;
}

int str_length(char *str)
{
	int i, l=0;
	
	for (i = 0; str[i]; i++)
	{
		l++;
	}
	
	if (l > 10)
	{
		return 1;
	}
	else
	{
		return l;
	}
}