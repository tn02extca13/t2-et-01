//int str_copy(char *str1, char *str2) - copies string 2 into string 1, returns 0 on success or -1 on error

#include <stdio.h>
 
int str_copy(char* str1,char* str2);
 
int main()
{
    char str1[10], str2[10], p;
    
    printf("\nEnter string: "); 
    scanf("%[^\n]s",str1);
    
    p = str_copy(str1,str2);
    
    if (p == 0)
	{
		printf("Success. \n");
	}
	else 
	{
		printf("Error! Upto only 10 characters.\n");
	}
    return 0;
}

int str_copy(char* str1,char* str2)
{
    int i=0, l=0;
    while (str1[i]!='\0')
    {
        str2[i] = str1[i];
        i++;
    }
    str2[i]='\0';
	
	for (i = 0; str2[i]; i++)
	{
		l++;
	}
	
	if (l > 10)
	{
		return 1;
	}
	else
	{
		printf("Copied in new string: %s\n",str2);
		return 0;
	}
}