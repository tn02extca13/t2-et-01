//Create a program to input a set of integers and sort them in ascending or descending order depending on users choice

#include<stdio.h>

void ascending(int x);
void descending(int x);

int array[50];

void main ()
{
	int n;
	char c;
	printf("Enter the number of elements in array[up to 50]: ");
	scanf("%d", &n);
	
	printf("Enter the %d elements of array:\n",n);
	for(int i=0; i<n; i++)
	{
		scanf("%d", &array[i]);
	}
	
	printf("What do you want Ascending or Descendin order [A/D]: ");
	scanf(" %c", &c);
	
	if (c == 'A')
	{
		ascending(n);
	}
	else if (c == 'D')
	{
		descending(n);
	}
	else
	{
		printf("Please write the valid order command!\n");
	}
}

void ascending(int n)
{
	int temp;
	for (int i = 0; i < n; i++)
	{
		for (int j = i + 1; j < n; j++)
		{
			if(array[i] > array[j])
			{
				temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
			
		}
	}
	printf("Array of Elemenst in Ascending Order are:\n");
	for (int i = 0; i < n; i++)
	{
		printf("%d  ", array[i]);
	}
	printf("\n");
}

void descending(int n)
{
	int temp;
	for (int i = 0; i < n; i++)
	{
		for (int j = i + 1; j < n; j++)
		{
			if(array[i] < array[j])
			{
				temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
			
		}
	}
	printf("Array of Elemenst in Descending Order are:\n");
	for (int i = 0; i < n; i++)
	{
		printf("%d  ", array[i]);
	}
	printf("\n");
}

/*
OUTPUT:

c:\Mycode>gcc ascending_or_descending.c

c:\Mycode>a.exe
Enter the number of elements in array[up to 50]: 5
Enter the 5 elements of array:
2
7
9
4
0
What do you want Ascending or Descendin order [A/D]: A
Array of Elemenst in Ascending Order are:
0  2  4  7  9

c:\Mycode>a.exe
Enter the number of elements in array[up to 50]: 5
Enter the 5 elements of array:
6
3
1
0
9
What do you want Ascending or Descendin order [A/D]: D
Array of Elemenst in Descending Order are:
9  6  3  1  0

c:\Mycode>a.exe
Enter the number of elements in array[up to 50]: 5
Enter the 5 elements of array:
1
2
8
7
9
What do you want Ascending or Descendin order [A/D]: p
Please write the valid order command!

c:\Mycode>

*/