//Function for finding and printing all factors of a number

#include<stdio.h>
int main() {
	int n,i=1;
	printf("enter no:");
	scanf("%d",&n);
	printf("\n the factors of number %d are:",n);
	while(i<=n)
	{
		if(n%i==0)
			printf("%d\n",i);
		i++;
	}
	return 0;
}