//Function for finding and printing all prime factors of a number

#include<stdio.h>

void prime_factor(int n);

void main ()
{
	int n;
	printf("\nEnter the number: ");
	scanf("%d",&n);
	
	prime_factor(n);
	
}

void prime_factor(int n)
{
	int p,i,j;
	printf("Prime factor of %d are:\n",n);
	
	for(i=2; i<=n; i++)
	{
		if(n%i == 0)
		{
			p = 1;
			for(j=2; j<=i/2; j++)
			{
				if(i%j == 0)
				{
					p = 0;
					break;
				}
			}
			if(p == 1)
			{
				printf("%d  ",i);
			}
		}
	}
	printf("\n");
}

/*
OUTPUT:

c:\Mycode\Milestone 02>gcc prime_factor.c

c:\Mycode\Milestone 02>a.exe

Enter the number: 3000
Prime factor of 3000 are:
2  3  5

c:\Mycode\Milestone 02>a.exe

Enter the number: 2550
Prime factor of 2550 are:
2  3  5  17

c:\Mycode\Milestone 02>

*/