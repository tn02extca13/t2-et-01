//A function to get LCM of 3 numbers

#include<stdio.h>
int main() {
	int n1,n2,n3,LCM,lcm(int n1,int n2,int n3);
	printf("enter 3 numbers:");
	scanf("%d%d%d",&n1,&n2,&n3);
	LCM=lcm(n1,n2,n3);
	printf("LCM of no. %d %d %d is %d",n1,n2,n3,LCM);
	return 0;
}

int lcm(int a,int b,int c){	
	int LCM;
	if(a>b & b>c)
		LCM=a;
	else if(b>a & a>c)
		LCM=b;
	else
		LCM=c;
	while(LCM%a!=0 || LCM%b!=0 || LCM%c!=0)
	{
		LCM++;
	}
	return LCM;
}