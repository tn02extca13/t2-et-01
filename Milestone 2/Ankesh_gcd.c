
//int gcd(int a, int b) - Calculates and returns the Greatest Common divisor of a and b

#include<stdio.h>
  int main() {
	int a,b,c,gcd(int a,int b);
	printf("enter a & b:");
	scanf("%d%d",&a,&b);
	c=gcd(a,b);
	if(c==1)
		printf("Greatest Common divisor does not exists");
	else
	    printf("greatest common divisor of %d & %d is =%d",a,b,c);
	return 0;
}

int gcd(int a,int b)
{
	 int gcd;
	 if(a<b)
		 gcd=a;
	 else
		 gcd=b;
	 while(a%gcd!=0 || b%gcd!=0)
	 {
		gcd--;	 
	 }
	 return gcd;
 }
