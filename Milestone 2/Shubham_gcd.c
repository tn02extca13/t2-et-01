//int gcd(int a, int b) - Calculates and returns the Greatest Common divisor of a and b

#include<stdio.h>
#include<math.h>

int gcd(int a, int b);

int main()
{
	int a,b;
	printf("Enter value of a: ");
	scanf(" %d",&a);
	printf("Enter value of b: ");
	scanf(" %d",&b);
	
	int c = gcd(a,b);
	printf("GCD value: %d\n",c);
	
	return 0;
}

int gcd(int a, int b)
{
	while(a!=b)
    {
        if(a > b)
            a -= b;
        else
            b -= a;
	}
	return a;
}

/*
OUTPUT:

c:\Mycode>gcc gcd.c

c:\Mycode>a.exe
Enter value of a: 9
Enter value of b: 15
GCD value: 3

c:\Mycode>

*/