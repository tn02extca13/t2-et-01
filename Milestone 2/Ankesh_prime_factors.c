//Function for finding and printing all prime factors of a number

#include<stdio.h>
int main() {
	int n,i=2,isprime,prime(int a);
	printf("enter no:");
	scanf("%d",&n);
	printf("\nthe prime factors of number %d are:",n);
	while(i<=n)
	{	if(n%i==0)
		{
			isprime=prime(i);
			if(isprime==1)
				printf("%d\n",i);
		}	
	    i++;
	}
	return 0;
}

int prime(int a){
int j=2;	
	while(a%j!=0)
	{
		j++;
	}	
	if(j==a)
		return 1;
	else
		return 0;
}