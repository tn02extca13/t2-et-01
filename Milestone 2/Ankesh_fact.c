//unsigned int fact(unsigned int n) - Calculates and returns the factorial of n.

#include<stdio.h>
int main() {
	int n,factorial;
	unsigned int fact(int n);
	printf("enter numbers:");
	scanf("%d",&n);
	if(n<0){
		printf("factorial of negative number does not exists.");
	}
	else{
		factorial=fact(n);
		printf("factorial of %d is %d",n,factorial);
	}
	return 0;
}

unsigned int fact(int n){
	int i,fact=1;
	for(i=n;i>=1;i--)
	{
		fact=fact*i;
	}
	return fact;
}