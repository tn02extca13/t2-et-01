//A function to get LCM of 3 numbers

#include<stdio.h>
#include<math.h>

int lcm(int a, int b);

int main ()
{
	int a,b,c,l,k;
	
	printf("\nEnter the 1st number: ");
	scanf("%d", &a);
	
	printf("Enter the 2nd number: ");
	scanf("%d", &b);
	
	printf("Enter the 3rd number: ");
	scanf("%d", &c);
	
	if(a>b)
	{
		l = lcm(a,b);
	}
	else
	{
		l = lcm(b,a);
	}
	if(l>c)
	{
		k = lcm(l,c);
	}
	else
	{
		k = lcm(c,l);
	}
	
	printf("LCM of %d, %d and %d is %d.\n",a,b,c,k);
	
}

int lcm(int a, int b)
{
	int temp=a;
	while(1)
	{
		if(temp%b == 0 && temp%a == 0)
		{
			break;
		}
		temp++;
	}
	return temp;
}

/*
OUTPUT:

c:\Mycode>gcc lcm.c

c:\Mycode>a.exe

Enter the 1st number: 20
Enter the 2nd number: 15
Enter the 3rd number: 30
LCM of 20, 15 and 30 is 60.

c:\Mycode>

*/