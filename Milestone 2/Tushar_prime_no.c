//int is_prime(unsigned int x) - Returns 1 if x is a prime number and returns 0 if it is not

#include<stdio.h>
#include<math.h>

int is_prime(unsigned int x);

int main()
{
	unsigned int x;
	
	printf("\nEnter the number: ");
	scanf("%u",&x);
	
	int p = is_prime(x);
	
	if (p == 1)
	{
		printf("%d is a prime number\n",x);
	}
	else
	{
		printf("%d is not a prime number\n",x);
	}
	
	return 0;
}

int is_prime(unsigned int x)
{
	int i;
	for (i = 2; i < x; i++)
	{
		if (x%i == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}

/*
OUTPUT:

c:\Mycode>gcc prime_no.c

c:\Mycode>a.exe

Enter the number: 7
7 is a prime number

c:\Mycode>a.exe

Enter the number: 4
4 is not a prime number

c:\Mycode>

*/