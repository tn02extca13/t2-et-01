//nCr - Combinations
#include<stdio.h>
int main() {
	int n,r,nCr,fact(int n);
	printf("enter n & r:");
	scanf("%d%d",&n,&r);
	nCr=fact(n)/(fact(n-r)*fact(r));
	printf("%dC%d is %d",n,r,nCr);
	return 0;
}

int fact(int a)
{
	int i,fact=1;
	for(i=a;i>=1;i--)
	{
		fact=fact*i;
	}
	return fact;
}