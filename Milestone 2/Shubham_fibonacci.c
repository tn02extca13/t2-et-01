//void fibonacci(int n) - Prints the first n fibonacci numbers

#include<stdio.h>
#include<math.h>

void fibonacci(int n);

void main ()
{
	int n;
	printf("Enter the value of n: ");
	scanf("%d",&n);
	
	fibonacci(n);
}

void fibonacci(int n)
{
	int i, a=0, b=1, c=0;
	
	printf("Fibnacci number of %d:\n",n);
	for (i=0; i<n; i++)
	{
		printf("%d  ",c);
		a=b;
		b=c;
		c=a+b;
	}
	printf("\n");
}

/*
OUTPUT:

c:\Mycode>gcc fibonacci.c

c:\Mycode>a.exe
Enter the value of n: 10
Fibnacci number of 10:
0  1  1  2  3  5  8  13  21  34
c:\Mycode>

*/