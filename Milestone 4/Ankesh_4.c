//Milestone 4

#include<stdio.h>
#include<stdlib.h>

int addElement(int array[], int size, int n);
void searchElement(int array[], int size);
int removeElement(int array[],int n);
void sortArray(int n);
void printElement(int n);

int array[10],size,n;

void main ()
{
	int choice,r;
	size = sizeof(array)/sizeof(array[0]);
	printf("\nEnter the number of elements in array[up to 10]: ");
	scanf("%d", &n);
	printf("\nEnter the %d elements of array:\n",n);
	for(int i=0; i<n; i++)
	{
		scanf("%d", &array[i]);
	}
	
	do
	{
		printf("\nSelect your choice:\n");
		printf("\t1. Add an element in the array\n");
		printf("\t2. Find an element exists in the array\n");
		printf("\t3. Remove an element from the array\n");
		printf("\t4. Sorting of the array\n");
		printf("\t5. Print all elements in the array\n");
		printf("\t6. Exit\n");
	
		scanf("%d",&choice);

		switch (choice)
		{
			case 1:
			n = addElement(array,size,n);
			break;
		
			case 2:
			searchElement(array,size);
			break;
		
			case 3:
			n = removeElement(array,n);
			break;
		
			case 4:
			sortArray(n);
			break;
		
			case 5:
			printElement(n);
			break;
		
			case 6:
			break;
		
			default:
			printf("Invalid Option. Please try again\n");
		}
	}
	while(choice != 6);
}

//1. Add an element in the array
int addElement(int array[], int size, int n)
{
	int i,add;
	if(n == 10)
	{
		printf("\nArray Full\n");
		return n;
	}
	else
	{
		printf("Enter the element: ");
		scanf("%d",&add);
		for(i=0; i<=size; i++)
		{
			if(add == array[i])
			{
				printf("Element already added. Please try again.\n");
				return n;
			}
			else
			{
				continue;
			}
		}
		printf("\nElement added successfully\n");
		array[n] = add;
		n++;
		return n;
	}
}

//2. Find an element exists in the array
void searchElement(int array[], int size)
{
	int i,t,f=0;
	
	printf("\nEnter element to search: ");
	scanf("%d", &t);
	
	for(i=0; i<size; i++)
	{
		if(array[i] == t)
		{
			f = 1;
			break;
		}
	}
	if(f==1)
	{
		printf("\nFound %d entered by user at index %d.\n",t,i+1);
	}
	else
	{
		printf("%d not found in the array.\n",t);
	}
}

//3. Remove an element from the array
int removeElement(int array[], int n)
{
	int i,element,found=0,pos;
	
	printf("\nEnter element to remove: ");
	scanf("%d",&element);
	
	for(i = 0; i < n; i++)
    {
        if(array[i] == element)
        {
            found = 1;
            pos = i;
            break;
        }
    }
	
    if(found == 1)
    {
        for(i = pos; i < n-1; i++)
		{
			array[i] = array[i+1];
		}
		printf("\nElement removed\n");
		n = n-1;
		return n;
    }
    else
    {
		printf("\n%d not removed\n", element);
		return n;
	}
}

//4. Sorting of the array
void sortArray(int n)
{
	int c,temp;

	do
	{
		printf("\nSelect the one of the below option:\n");
		printf("\t1. Ascending\n");
		printf("\t2. Descending\n");
		scanf(" %d", &c);
		
		switch (c)
		{
			case 1:
			for (int i = 0; i < n; i++)
			{
				for (int j = i + 1; j < n; j++)
				{
					if(array[i] > array[j])
					{
						temp = array[i];
						array[i] = array[j];
						array[j] = temp;
					}
			
				}
			}
			printf("Array of Element in Ascending Order are:\n");
			for (int i = 0; i < n; i++)
			{
				printf("%d\t", array[i]);
			}
			printf("\n");
			break;
	
			case 2:
			for (int i = 0; i < n; i++)
			{
				for (int j = i + 1; j < n; j++)
				{
					if(array[i] < array[j])
					{
						temp = array[i];
						array[i] = array[j];
						array[j] = temp;
					}
				}
			}
			printf("Array of Element in Descending Order are:\n");
			for (int i = 0; i < n; i++)
			{
				printf("%d\t", array[i]);
			}
			printf("\n");
			break;
			
			default :
			printf("\nPlease write the valid order command!\n");
			break;
		}
	}while(c<=0 || c>=3);
}

//5. Print all elements in the array
void printElement(int n)
{
	printf("\nElements in the array:\n");
	for(int i=0; i<n; i++)
	{
		printf("%d\t", array[i]);
	}
	printf("\n");
}