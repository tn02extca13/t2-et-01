//Milestone 4

#include<stdio.h>

void addElement(int size);

int array[50];

void main ()
{
	int choise,size,n;
	size = sizeof(array)/sizeof(array[0]);
	
	printf("\nEnter the number of elements in array[up to 50]: ");
	scanf("%d", &n);
	
	printf("\nEnter the %d elements of array:\n",n);
	for(int i=0; i<n; i++)
	{
		scanf("%d", &array[i]);
	}
	
	do
	{

		printf("\n1. Addition of an element in the array.\n");
		printf("2. Find an element exists in the array\n");
		printf("3. Remove an element from the array\n");
		printf("4. Sorting of the array\n");
		printf("5. Print all elements in the array\n");
		printf("6. Exit\n");
	
		printf("\nChoose The Option: ");
		scanf("%d",&choise);

		switch (choise)
		{
			case 1:
			addElement(size);
			break;
		
			case 2:
			//searchElement(array,size);
			break;
		
			case 3:
			//removeElement(array,size);
			break;
		
			case 4:
			//sortArray(n);
			break;
		
			case 5:
			//printElement(n);
			break;
		
			case 6:
			break;
		
			default:
			printf("Invalid Option. Please try again\n");
		}
	}while(choise <6 || choise >=7);
}

void addElement(int size)
{
	int i, sum=0;
	for(i=0;i<size;i++)
		sum += array[i];
	
	printf("%d",sum);
}